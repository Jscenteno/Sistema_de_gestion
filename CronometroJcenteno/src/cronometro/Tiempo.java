package cronometro;


public class Tiempo {
    private Integer hora;
    private Integer minuto;
    private Integer segundo;
    private Integer milisec;
    
    public Tiempo(){
        hora=0;
        minuto=0;
        segundo=0;
        milisec=0;
    }
    
    public void setHora(int hora){
        this.hora=hora;
    }
    public void setMinuto(int minuto){
        this.minuto=minuto;
    }
    public void setSegundo(int segundo){
        this.segundo=segundo;
    }
    public void setMilisec(int milisec){
        this.milisec=milisec;
    }
    
    public Integer getHora(){
        return hora;
    }
    public Integer getMinuto(){
        return minuto;
    }
    public Integer getSegundo(){
        return segundo;
    }
    public Integer getMilisec(){
        return milisec;
    }
}