package cronometro;

public class Reloj implements Runnable{
    
    Miventanita vis;
    
    public void run(){
        Tiempo time=new Tiempo();
        while(true){
            
            int msec=time.getMilisec();
            int sec=time.getSegundo();
            int min=time.getMinuto();
            int hora=time.getHora();
            try{
                Thread.sleep(1000); 
            }
            catch(InterruptedException e){

            }
            msec++;
            if(msec>=100){
                sec++;
               msec=0;
          }
            sec++;
            if(sec>=60){
                min++;
                sec=0;
            }
            if(min>=60){
                hora++;
                min=0;
            }
            if(hora>=23){
                hora=0;
            }

            if(vis.getAction())
                time=new Tiempo();

            if(vis.getEstado()){

                time.setHora(hora);
                time.setMilisec(msec);
                time.setMinuto(min);
                time.setSegundo(sec);

                vis.Acomodar(time); 
            }
        }
    }
    
    public Reloj(Miventanita vista){
        vis=vista;
    }
}