package cronometro;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Miventanita extends JFrame {
    JPanel p1; 
    JPanel p2; 
    JPanel p3;
    JLabel hr, min, sec, msec, dot1, dot2;
    JButton start, restart, stop, resume;
    private boolean state=false;
    private boolean action=false;
    
    public Miventanita(){
        super("ventana de cronometro");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setBounds(50, 50, 480, 300);
        
        p1=new JPanel(new GridLayout(1,5));
        p3=new JPanel();
        hr=new JLabel("00"); 
        min=new JLabel("00");
        sec=new JLabel("00"); 
        msec=new JLabel("00");
        dot1=new JLabel(":"); dot2=new JLabel(":");
        hr.setFont(new Font("Arial", Font.BOLD, 70));
        min.setFont(new Font("Arial", Font.BOLD, 70));
        sec.setFont(new Font("Arial", Font.BOLD, 70));
        dot1.setFont(new Font("Arial", Font.BOLD, 70));
        dot1.setBounds(140,60,100,100);
        dot2.setFont(new Font("Arial", Font.BOLD, 70));
        dot2.setBounds(280,60,100,100);
        p1.add(hr);
        add(dot1);
        p1.add(min);
        add(dot2);
        p1.add(sec);
        msec.setBounds(420, 85,100,100);
        add(msec);
        this.add(p1, BorderLayout.CENTER);
        p3.setPreferredSize(new Dimension(44,44));
        this.add(p3, BorderLayout.WEST);
        
        p2=new JPanel(new FlowLayout());
        
        start=new JButton("Iniciar"); start.setBounds(100, 20, 100, 20);
        restart=new JButton("Reiniciar"); restart.setBounds(100, 20, 100, 20);
        stop=new JButton("Detener"); stop.setBounds(100, 20, 100, 20);
        resume=new JButton("Continuar"); resume.setBounds(100, 20, 100, 20);
        p2.add(start); p2.add(restart);
        p2.add(stop); p2.add(resume);
        this.add(p2,BorderLayout.SOUTH);
       
        start.setEnabled(true);
        stop.setEnabled(true);
        restart.setEnabled(true);
        resume.setEnabled(true);
        
        this.setVisible(true);
        
        start.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                state=true;
                start.setEnabled(true);
                stop.setEnabled(true);
                restart.setEnabled(false);
                resume.setEnabled(false);
             
	 }
	});
        
        stop.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                state=false;
                stop.setEnabled(true);
                restart.setEnabled(true);
                resume.setEnabled(true);
	 }
	});
        
        restart.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                hr.setText("00");
                min.setText("00");
                sec.setText("00");
                msec.setText("00");
                
                restart.setEnabled(true);
                start.setEnabled(true);
                resume.setEnabled(true);
                action=true;
	 }
	});
        
        resume.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                state=true;
                stop.setEnabled(true);
                resume.setEnabled(true);
                restart.setEnabled(false);
	 }
	});
    }
    
    public boolean getEstado(){
        return state;
    }
    public boolean getAction(){
        return action;
    }
    
    public void Acomodar(Tiempo time){
        
        if(time.getHora()<10)
            hr.setText("0"+time.getHora().toString());
        else
            hr.setText(time.getHora().toString());
        
        if(time.getMilisec()<10)
            msec.setText("0"+time.getMilisec().toString());
        else
            msec.setText(time.getMilisec().toString());
        
        if(time.getMinuto()<10)
            min.setText("0"+time.getMinuto().toString());
        else
            min.setText(time.getMinuto().toString());
        
        if(time.getSegundo()<10)
            sec.setText("0"+time.getSegundo().toString());
        else
            sec.setText(time.getSegundo().toString());
        
        action=false;
    }
    
}