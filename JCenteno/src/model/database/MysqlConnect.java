package model.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlConnect {

	Connection conn = null;
	public void connect() {
		try {
			conn = DriverManager.getConnection
					("jdbc:mysql://localhost:3306/prueba?"
							+ "useUnicode=true&"
							+ "useJDBCCompliantTimezoneShift=true&"
							+ "useLegacyDatetimeCode=false&"
							+ "serverTimezone=UTC","root","");
			System.out.println("Success");
			
			
			
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		
	}
	
	public Connection getConn() {
		return conn;
	}

	public static  void main(String args[])  {
		MysqlConnect conn = new MysqlConnect();
		conn.connect();
		
		
			
			}
		
}